#A Keegan Shudy file

from Bio.PDB import PDBParser
import matplotlib
import matplotlib.pyplot as plot
def main():
	matplotlib.use('Agg')
	parser = PDBParser()
	structure = parser.get_structure("1kfj", "1kjf.pdb")
	#model = structure[0]
	#chain = model['A']
	#residue = chain[(' ', 1, ' ')]
	#carbon = residue['CA']
	#nitrogen = residue['N']

	#print carbon.name
	
	#print chain.id
	#print residue.id[1], residue.resname
	#print atom.name, atom.occupancy
	
	#residue_list = chain.child_list
	#print residue_list
	#for i in chain:
#		print i.resname
		
	ca = None
	n = None
	length = sum(1 for a in structure.get_atoms())
	struct = structure.get_atoms()
	distances = []
	for x,i in zip(xrange(length), structure.get_atoms()):
		if i.name == 'CA':
			ca = i
			for y,j in zip(xrange(x,length), structure.get_atoms()):
				if j.name == 'N':
					n = j
					distances.append(ca-n)


	plot.hist(distances, len(distances) / 10, facecolor='c')
	plot.xlabel('Distances')
	#plot.ylabel('Count')
	plot.title('Atom Distances')
	plot.grid(True) # Show grid lines
	#plot.savefig(os.path.join('data_files', 'blast_score_hist.png'))
	plot.show()
	plot.cla()
	plot.clf()

	#for i in structure.get_atoms():
	#	if i.name == 'CA':
	#		ca = i




if __name__=='__main__':main()
