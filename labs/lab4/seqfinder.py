"""
Keegan Shudy
Lab 4

Finds the number of 'ATG, TAA, TAG, TGA' in a sequence
Also finds all open reading frames
"""

import re
from string import maketrans
def main():
	full_file = open('sequence.fasta')
	title = full_file.readline().split("|")[4]
	full_file = ''.join(full_file)
	full_file = full_file.replace('\n','')

	codons = [full_file[x:x+3] for x in xrange(0, len(full_file), 3)]
	codon_dict = {}
	count = 0
	firstATG = 0
	first = True
	for codon in codons:
		if codon_dict.has_key(codon):
			codon_dict[codon] += 1
		else:
			codon_dict[codon] = 1
		if codon == 'ATG' and first:
			first = False
			firstATG = count
		count += 1
	print title
	print 'First ATG pos: ', firstATG * 3 
	print '# of ATGs: ', codon_dict['ATG']
	print '# of TAAs: ', codon_dict['TAA']
	print '# of TAGs: ', codon_dict['TAG']
	print '# of TGAs: ', codon_dict['TGA']
	print 'Starting opening frame pos: %d length: %d'% (full_file.index(list(openframes(full_file))[0]), len(list(openframes(full_file))[0]))
	print openframes(full_file)

def openframes(dna):
	pattern = re.compile(r'(?=(ATG(?:...)*?)(?=TAG|TGA|TAA))')
	return set(pattern.findall(dna))


if __name__ == '__main__':main()