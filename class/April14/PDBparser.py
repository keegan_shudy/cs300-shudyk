#Keegan Shudy
#April 14
#Parsing a PDB file

from Bio.PDB import PDBParser
def main():

	parser = PDBParser()
	structure = parser.get_structure("1kfj", "1kjf.pdb")
	model = structure[0]
	chain = model['A']
	residue = chain[(' ', 1, ' ')]
	atom = residue['CA']
	
	print chain.id
	print residue.id[1], residue.resname
	print atom.name, atom.occupancy
	
	#residue_list = chain.child_list
	#print residue_list
	for i in chain:
		print i.resname
		
	for i in structure.get_atoms():
		print i.name


if __name__=='__main__':main()
