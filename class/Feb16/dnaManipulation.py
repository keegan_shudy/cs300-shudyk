from string import maketrans

def main():
	DNA  = raw_input('Enter a DNA sequence:')
	DNA = DNA.upper()
	order = raw_input("3' -> 5' (1) or 5' -> 3' (2):")
	template = raw_input('Template strand (y or n): ')

	if template.lower() == 'y' and order == '1':
		print "3' ",DNA, " 5'"
	elif template.lower() == 'y' and order == '2':
		print "3' " , DNA[::-1] , " 5'"
	elif template.lower() == 'n' and order == '2':
		print "3' ",DNA.translate(maketrans('ACTG', 'TGAC'))," 5'"
	elif template.lower() == 'n' and order == '1':
		print "5' ", DNA[::-1].translate(maketrans('ACTG', 'TGAC')), " 3'"



if __name__ == '__main__':main()